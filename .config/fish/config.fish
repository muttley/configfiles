set -xU LC_COLLATE C
set -xU EDITOR micro

function fish_prompt --description 'Write out the prompt'
    powerline-go -error $status -shell bare -modules venv,user,host,ssh,cwd,perms,git,hg,jobs,exit,root
end

set -U fish_prompt_pwd_dir_length 0

set GIT_EDITOR micro
alias nano=micro
alias mj='make -j8'

set -xU DOTNET_CLI_TELEMETRY_OPTOUT 1 2>/dev/null

alias ls='ls -Ah --group-directories-first --color'
source ~/.config/fish/fish_command_timer.fish
set fish_command_timer_time_format '%d-%m-%Y %H:%M:%S'

alias micvolset='amixer -c 0 set Mic 28'
alias enablerightmonitor='xrandr --output HDMI-1-2 --mode 1680x1050 --right-of DVI-D-0'
alias disableright='xrandr --output HDMI-2 --off'
alias enableright='xrandr --output HDMI-2 --mode 1680x1050 --right-of HDMI-1'
alias wildanimalracing='xrandr --output HDMI-2 --off; sleep 1; steam -applaunch 389510; sleep 5; xrandr --output HDMI-2 --mode 1680x1050 --right-of HDMI-1'
